import gitlab
import urllib3
import time
import os

Project_with_500_error = ["ST_core_import","configserver"]
class GitLab_API:

    def __init__(self, cfg_name, gl_cfg, fp):
        try:
            self.gl = gitlab.Gitlab.from_config(cfg_name, [gl_cfg])
            self.gl.timeout = 100
            self.fp = fp
        except Exception as e:
            print("GitLab server authentication error ", str(e))

    def get_active_users(self):
        try:
            self.active_users = []
            for user in self.gl.users.list(iterator=True):
                if user.state == "active":
                    self.active_users.append(user)
                    print("List of Active users: ", self.active_users)
        except:
            print("GitLab error in user count ")
    def get_group_count(self):
        try:
            print("Number of groups: ", len(self.gl.groups.list(get_all=True)))
            self.fp.write("Number of groups: %s \n"% str(len(self.gl.groups.list(get_all=True))))
        except Exception as err:
            print("GitLab error in group count ", str(err))
    def get_commit_data(self):
        try:
            print("inside get_commit_data 33")
            for group in self.gl.groups.list(get_all=True):
                print("inside forloop 35")
                group_id = group.attributes['id']
                print("="*50)
                print("GROUP ID: ", group_id)
                print("=" * 50)
                self.fp.write("=" * 50)
                self.fp.write('\n')
                self.fp.write("GROUP ID: %s \n" %(group_id))
                self.fp.write("=" * 50)
                self.fp.write('\n')
                group = self.gl.groups.get(group_id)
                projects = group.projects.list(get_all=True)
                print("==Group projects==")
                self.fp.write("==Group projects==\n")
                for grp_proj in projects:
                    proj = self.gl.projects.get(grp_proj.id)
                    print("Project Name: ", proj.name)
                    self.fp.write("Project Name: %s \n"%proj.name)
                    if proj.name in Project_with_500_error:
                        continue
                    print("Last activity at: ", grp_proj.last_activity_at)
                    self.fp.write("Last activity at:  %s \n"%(grp_proj.last_activity_at))
                    print("Last commit info: ", proj.commits.list(per_page=1))
                    self.fp.write("Last commit info: %s \n"%(proj.commits.list(per_page=1)))
                    print("Project files:")
                    print(proj.files)
                    print("Project repos:")
                    try:
                        for repo in proj.repository_tree(all=True):
                            if repo:
                                print(repo)
                            if repo.get('name') == ".gitlab-ci.yml":
                                print("Project ", proj.name, " is CI/CD enabled ")
                                ci_proj_str = "Project " + proj.name + " is CI/CD enabled \n"
                                self.fp.write(ci_proj_str)
                            else:
                                continue
                    except gitlab.exceptions.GitlabGetError:
                        print("Repository not found")
                    except gitlab.exceptions.GitlabAuthenticationError:
                        print("Repository does not exist 500++++++++++++++")
        except Exception as err2:
            print("GitLab error in get_commit_data ", str(err2))

if __name__ == "__main__":
    timestr = time.strftime("%Y%m%d-%H%M%S")
    file_name = "gitlab_info_" + timestr + ".txt"
    urllib3.disable_warnings()
    with open(file_name,'a') as fp:
        gl_obj = GitLab_API("personal", 'gl.cfg', fp)
        # gl_obj.get_active_users()
        # gl_obj.enable_debug()
        gl_obj.get_group_count()
        gl_obj.get_commit_data()
        # GitLab_API.enable_debug()

    with open(file_name,'r') as fp:
    # with open(os.path.join(os.path.dirname(__file__), file_name)) as fp:
        print (fp.read())
