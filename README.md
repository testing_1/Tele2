1. Created 1 group named testing_1 and 1 project named Tele2.
2. imported the given python code in Tele2 project.
3. Created .gitlab-ci.yml and configured the pipeline build stages (mostly of python and its modules             configuration) and also artifacts section.
4. Configured Gitab API url, private access token and used that in gl.cfg file.
5. Able to run the python script using pipeline. Logs are getting visible in the pipeline jobs console.
But the final output file is not generating in the Output directory. 
6. Currently I am checking that.
Note: In project Tele2 CICD is enabled


    
